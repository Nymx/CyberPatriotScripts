::enables automatic updates and firewall
pause
reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update" /v AUOptions /t REG_DWORD /d 0 /f
netsh advfirewall set allprofiles state on
::finds all media files and stores them in file
Pause
CD User
dir *.mp3 *.png *.tif *.jpg *.gif *.aif *.iff *.m3u *.m4a *.mid *.mpa *.ra *.wav *.wma *.3g2 *.3gp *.asf *.asx *.avi *.flv *.m4v *.mov *.mp4 *.mpg *.rm *.srt *.swf *.vob *.wmv /s /b /a h > mediafiles.txt
Notepad.exe mediafiles.txt
CD ..
:: creates file that has security/password policy and then updates it
pause
secedit.exe /export /cfg C:\secconfig.cfg
Notepad.exe C:\secconfig.cfg
::when file is updated
pause
secedit.exe /configure /db %windir%\securitynew.sdb /cfg C:\secconfig.cfg /areas SECURITYPOLICY
::Disable Guest
Pause
net user guest /active:no
::
Pause