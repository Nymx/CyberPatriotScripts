#!/bin/sh
#title			:unix_script.sh
#description	:This script implements the unix checklist
#author			:Lachlan Sneff
#date			:02/25/17
#version		:0.1
#usage			:./unix_script.sh

ROOT_UID=0
SSH_CONFIG=/etc/ssh/sshd_config		# path the sshd config
GUEST_DISABLE=/usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf
COMMON_PASSWORD=/etc/pam.d/common-password
LOGIN_DEFS=/etc/login.defs
COMMON_AUTH=/etc/pam.d/common-auth

LOG_FILE=unix.log
USERS_FILE=users.txt
RKHUNTER_LOG=/var/log/rkhunter.log

#make self-printing log file & clear file
echo "#!/bin/cat" > $LOG_FILE
chmod +x "$LOG_FILE"

log ()
{
	echo "[$(date +%S:%M:%H)] $*" | tee -a "$LOG_FILE"

	return 0
}

install ()
{
	apt-get install -y "$*" > /dev/null 2>&1
}

uninstall ()
{
	apt-get remove -y "$*" > /dev/null 2>&1
}

# Run as root
if [ $(id -u) -ne $ROOT_UID ]
then
	log "Must be root to run this script."
	exit 1
fi

#install figlet for a cool banner
install figlet
if [ $? -eq 1 ]
then
	log "Failed to install figlet, defaulting to no banner"
else
	figlet "CyberScript"
fi

# Here starts the actual script

# TODO: Parse users in the readme
#+ then delete users
#+ for now, supply legit users in users.txt
for user in $(echo /home/* | cut -d'/' -f3)
do
	# checks if users.txt contains each user that has a home directory
	if [ $(python configparser.py "$USERS_FILE" "*" | grep -Fxq "$user") -ne 0 ] # if users.txt does not contain user
	then
		userdel -rf "$user"
		if [ $? -ne 0 ] # failed to remove
		then
			log "Failed to remove $user"
		fi
	fi
done
log "Deleted all illegitimate users"

# set correct privledges for each user
for user in $(python configparser.py "$USERS_FILE" "admin")
do
	usermod -a -G sudo user
done
for user in $(python configparser.py "$USERS_FILE" "std")
do
	# remove user from sudo group
	deluser "$user" sudo
done
log "Fixed priviledges for all users"

# disable ssh root login
sed -i '/#PermitRootLogin/c\PermitRootLogin no' "$SSH_CONFIG"
log "Disabled ssh root login"

# disable root account
passwd -l root
log "Locked root account"

#TODO: enable daily updates

#TODO: set update sources

# pull in new updates
apt-get update

# install and enable firewall
install ufw
ufw enable

# disable guest account
if [ $(grep -Fxq "allow-guest=false" "$GUEST_DISABLE") -ne 0 ]
then
	echo "allow-guest=false" >> "$GUEST_DISABLE"
fi
log "Disabled guest login"

# PAM
#install cracklib
install libpam-cracklib
# append remember=5
sed '/pam_unix.so/s/$/ remember=5 minlen=8/' "$COMMON_PASSWORD"
sed '/pam_cracklib.so/s/$ ucredit=-1 lcredit=-1 dcredit=-1 ocredit-1/' "$COMMON_PASSWORD"

# set password age requirements
sed -i '/PASS_MAX_DAYS/c\PASS_MAX_DAYS 90' "$LOGIN_DEFS"
sed -i '/PASS_MIN_DAYS/c\PASS_MIN_DAYS 10' "$LOGIN_DEFS"
sed -i '/PASS_WARN_AGE/c\PASS_WARN_AGE 7' "$LOGIN_DEFS"

# set account lockout policy
echo "auth required pam_tally2.so deny=5 onerr=fail unlock_time=1800" >> "$COMMON_AUTH"

log "Setup pamlib"

# setup auditing
install auditd
auditctl -e 1

log "Enabled auditing"

log "Finished basic fixes, checking for backdoors"

install rkhunter
rkhunter --update
log "Running rkhunter"
rkhunter --sk --check > /dev/null 2>&1

# check rkhunter.log for warnings

log "Items found by rkhunter: (Not all may be malicious)"
for warning in $(grep "Warning: " "$RKHUNTER_LOG")
do
	echo "$warning"
done

# check for users other than root with a uid of 0
for user in $(grep ':0:' /etc/passwd | cut -d : -f1)
do
	if [ "$user" -ne "root" ]
	then
		log "Found $user with uid of 0"
		log "Deleting $user"
		sed '/$user/d' "/etc/passwd" > "/etc/passwd"
	fi
done

# check cronjobs for backdoors
# This is a bit tough, often the backdoor will start nc,
#+ but there could be a renamed nc binary, or a script in another language
# A better approach might be searching through all processes that are listening on 0.0.0.0
#+ however, this could be easily avoided

log "Look through the cronjobs in /etc/crontab, /etc/cron.*/, rc.local"

log "Checking process list for processes that have the same hash as netcat"

NC_SHASUM=$(sha1sum `which netcat` | cut -d' ' -f1)
for pid in $(ps -Ao pid)
do
	if [ "$(sha1sum /proc/$pid/exe | cut -d' ' -f1)" -eq "$NC_SHASUM" ]
	then
		log "$pid has the same hash as netcat"
	fi
done

log "Uninstalling netcat*, nmap, john, aircrack"

uninstall "netcat*" "nmap" "john" "aircrack-ng"

log "Malicious software uninstalled"


#TODO: clamAV

#something something
sysctl -n net.ipv4.tcp_syncookies