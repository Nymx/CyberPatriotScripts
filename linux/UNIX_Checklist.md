Basic

* Remove unknown users

* Check user account type

* Add password to all accounts

* Disable root account

* SSH

    * Disable root login

* Set updates to daily

* Apt-get update

* Enable firewall

* Disable guest account

* PAM

    * Add "remember=5" to common-password

    * Add "minlen=8" to common-password

    * Enforce password complexity

    * Set Password age requirements

    * Add "auth required pam_tally2.so deny=5 onerr=fail unlock_time=1800" to common-auth

* Setup audits

* "Apt-get dist-upgrade"

Backdoors

* Download and run rkhunter and chkrootkit

* Show any lines with warning

* Delete non-root accounts with uid of 0

* Cron

    * For now, print out all jobs in /etc/cron.daily, /etc/cron.hourly, etc

    * Print out /etc/crontab

* Uninstall netcat*, nmap, john, aircrack-ng

* Check process list for suspicious processes

    * Binaries that have the same hash as netcat

    * Processes that are listening and present bash when connected to

    * Processes that in the /usr/bin dir, but have no /proc/pid/cmdline

* Download and run clamAV

Misc

* "sysctl -n net.ipv4.tcp_syncookies"

* Close ports that should be closed

